from itertools import count


def inct_fcn():
    if hasattr(inct_fcn, "num"):
        inct_fcn.num += 1
    else:
        inct_fcn.num = 0

    return inct_fcn.num


for _ in range(10):
    print(inct_fcn())


cnt = count(0, 1)


for _ in range(10):
    print(next(cnt))

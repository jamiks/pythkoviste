import py


class TestCls():
    def __init__(self, value):
        self.value = value
        self.enter_id = 0
        self.exit_id = 0

    def __enter__(self):
        self.enter_id += 1
        print(f"Entering the usage for the {self.enter_id} time")
        print(self.value)
        return self

    def __exit__(self, etype, value, traceback):
        self.exit_id += 1
        print(f"Exiting this context for {self.exit_id} time")


def test_class():
    with TestCls(123) as testik:
        print(testik.exit_id)
        assert testik.enter_id == 1
        assert testik.exit_id == 0
        testik.exit_id = 74
        assert testik.exit_id == 44
        

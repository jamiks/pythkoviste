import threading
import time

print("started")

def fcn():
    thread = threading.Thread(target=thread_fcn, daemon=True)
    thread.start()
    time.sleep(5)
    thread.join()

def thread_fcn():
    i = 0
    last = time.time()
    while True:
        time.sleep(1)
        if i >= 10:
            print('finished')
            return
        else:
            new = time.time()
            print(new - last - 1)
            last = new
            i+=1

fcn()

for i in range(15):
    time.sleep(0.5)
    print("n", end=" ")

time.sleep(10)

thread_fcn()

print("ended")

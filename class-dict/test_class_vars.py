import abc
import pdb

class Foo(abc.ABC):
    value = 1


class Bar(Foo):
    value2 = Foo.value + 1
    value3 = value2 + 120


if __name__=="__main__":
    bar=Bar
    pdb.set_trace()

import pytest
import abc

class AbstractCase(abc.ABC):
    value: int
    dict = {}

    @pytest.fixture(scope="class", autouse=True)
    def fix_fill_dict(self):
        for i in range(self.value):
            self.dict[i] = i+500
    
    def test_len(self):
        assert len(self.dict) == self.value
    
    def test_vals(self):
        for key, value in self.dict.items():
            assert value >= 500

class TestFew(AbstractCase):
    value = 2

class ManyTest(AbstractCase):
    value = 70
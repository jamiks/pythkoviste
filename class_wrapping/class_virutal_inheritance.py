class Parent:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def shoW(self):
        print(f"Class Parent: {self.a} {self.b}")

class VirtualChild:
    __metaclass__=Parent
    def __init__(self, c, d):
        self.c = c
        self.d = d
        self._parent = Parent(c, d)


class Child(Parent):
    def __init__(self, c, d):
        self.c = c
        self.d = d
        self._parent = Parent(c,d)

    def __getattribute__(self, name: str):
        getattr(self._parent, name)


print(issubclass(Child, Parent))
print(issubclass(VirtualChild, Parent))
import pdb
pdb.set_trace()
from abc import ABC, abstractclassmethod

"""What are requirement

- have class instance that is wrapper of already existing class which will be registered as the class in containment.
"""


class OriginalClass(object):
    def __init__(self, a, b):
        self.a = a
        self.b = b


class MetaClass(ABC):
    __name__ = "MetaClass"

    def __init__(self, subclass: OriginalClass, x) -> None:
        self.subclass = subclass
        self.x = x
    
    def get_x(self):
        print(self.x)


MetaClass.register(OriginalClass)


def suborigin(cls):

    cls.register(OriginalClass)
    class Wrapper(cls):
        def __init__(self, subclass: OriginalClass, x) -> None:
            self.subclass = subclass
            self.x = x

        def get_x(self):
            print(self.x)


    return Wrapper


@suborigin
class WrappedClass(ABC):
    @abstractclassmethod
    def __init__(self, subclass: OriginalClass, x):
        pass


def test_subclass(inst, cls):
    print(f"{inst} is instance of OriginalClass")
    print(f"{isinstance(inst, OriginalClass)}")
    print(f"{inst} is instance of {cls.__name__}")
    print(f"{isinstance(inst, cls)}")
    print(f"{cls.__name__} is subcls of OriginalClass")
    print(f"{issubclass(cls, OriginalClas )}")
    print(f"OriginalClass is subcls of {cls.__name__}")
    print(f"{issubclass(OriginalClass, cls)}")


mc = MetaClass(OriginalClass(1,'aa'), 'mc')
wc = WrappedClass(OriginalClass(1,'aa'), 'wc')

print("Testing metaclass:")
print(issubclass(OriginalClass, MetaClass))
print(issubclass(MetaClass, OriginalClass))

'''
print("Testing wrapped class:")
test_subclass(wc, WrappedClass)
mc.get_x()
wc.get_x()
'''
import pdb
pdb.set_trace()
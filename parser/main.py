'''
This script takes all text files from desired direcotory and translates them from one syntax to another defined by textboxlanguage in configuration file.
Attributes used:
-c --config <config.ini>    name of configuration file, by default it's config.ini
-o --out <path>             output folder (when not set, it will write the output into input. If same extension is defined no backup is made
-b --bak                    backups the files when same extension. If added, suffix *.bak is added to old files
-d --del                    deletes old files
'''
import os, sys

files=dict() # dictionary of folders and files: Structure is following:..



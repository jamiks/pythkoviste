import itertools
from time import time

with open("testfile.txt") as file:
    start=time()
    a=file.readlines()[1]
    stop=time()

print("readline")
print(a)
print(stop-start)

with open("testfile.txt") as file:
    start=time()
    a=next(itertools.islice(file, 1, 2))
    stop=time()

print("iteration")
print(a)
print(stop-start)

with open("testfile.txt") as file:
    start=time()
    next(file)
    a=next(file)
    stop=time()

print("nexting")
print(a)
print(stop-start)

with open("testfile.txt") as file:
    start=time()   
    a=enumerate(file)[2]
    stop=time()

print("nexting")
print(a)
print(stop-start)


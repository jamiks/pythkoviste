import pdb

output_str = "trump.bump.dump\ntrump.dump.gump\ntrump.dump.slur\nhtum.potm\nkkkom"

values = {
    "trump.bump.dump": 100,
    "trump.dump.gump": 200,
    "trump.dump.slur": 300,
    "htum.potm": 400,
    "nkkkom": 500
}


class TestObject:
    def __init__(self, input, parent = None):
        # first get first level objects:
        self._parent = parent
        
        names = [pathtext.split(".", maxsplit=1)[0] for pathtext in input.split('\n')  ]
        for pathtext in input.split('\n'):
            
        for name in names:
            subinput = [pathtext]
            self.__dict__[name] = TestObject(subinput, name)

    @property
    def value(self):
        return


test = TestObject(output_str)
pdb.set_trace()
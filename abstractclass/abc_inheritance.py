from abc import ABC


def get_parent(value):
    return ParentClass(value)


class ParentClass(object):
    def __init__(self, id, wx=False):
        self.name = "ParentClass"
        self.id = id
        self.wx = wx

    def check(self):
        print(f"Active values:\nid: {self.id}\nwx: {self.wx}")

    def set_defaults(self):
        self.id = 10
        self.wx = False
        print("Done")


class MyClass(ABC):
    def __init__(self, value):
        self.name = "MyClass"
        self.value = value
        self._parent = get_parent(value)

    def check(self):
        print(f"Active values:\nvalue: {self.value}\n_parent: {self._parent}")

    def __getattr__(self, attr):
        return getattr(self._parent, attr)

    @property
    def virtual(self):
        return 100


class OtherClass(ABC):
    def __init__(self, value):
        self.name = "OtherClass"
        self.value = value
        self._parent = get_parent(value)
        self.__metaclass__ = ParentClass

    def check(self):
        print(f"Active values:\nvalue: {self.value}\n_parent: {self._parent}")

    def __getattr__(self, attr):
        return getattr(self._parent, attr)

    @property
    def virtual(self):
        return 100


MyClass.register(ParentClass)

dirs = {}

for inst in [MyClass(1), OtherClass(1), ParentClass(1)]:
    print(inst.name)
    print(f"is subclass of Parent: {issubclass(inst.__class__, ParentClass)}")
    print(f"is instance of Parent: {isinstance(inst, ParentClass)}")
    print(f"is Parent its subclass {issubclass(ParentClass, inst.__class__)}")
    print(f"is Parent its instance: {isinstance(ParentClass(1), inst.__class__)}")
    inst_dir = inst.__dir__()
    inst_dir.sort()
    dirs[inst.name] = inst_dir
    #for attr in inst_dir:
    #    print(attr)

diffs = {}

for key1, values1 in dirs.items():
    for key2, values2 in dirs.items():
        if key2 != key1 and f"{key1}_{key2}" not in diffs:
            set_1 = set(values1)
            set_2 = set(values2)
            diffs[f"{key1} not in {key2}"] = set_1 - set_2
            diffs[f"{key2} not in {key1}"] = set_2 - set_1

print("///////////////////////////////////")

for key, value in diffs.items():
    print("********")
    print(key)
    print("## Actual Diff:")
    for i in value:
        print(i)

import abc
import pdb

class Abstrace(abc.ABC):
    string: str
    number: int

class Child(Abstrace):
    string = "hello dolly"

if __name__ == "__main__":
    pdb.set_trace()

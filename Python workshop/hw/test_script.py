import time
from my_decorators import check_time

@check_time
def timer(some_time):
    time.sleep(some_time)
    return True

timer(0.2)
timer(1.4)
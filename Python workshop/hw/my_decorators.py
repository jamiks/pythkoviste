import time

def check_time(some_function):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        output = some_function(*args, **kwargs)
        out_time = time.time() - start_time
        print(f'Time of {some_function} is {out_time} ms')
        return output
    return wrapper
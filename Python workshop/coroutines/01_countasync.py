#!/usr/bin/env python3
# countasync.py

import pdb
import asyncio


async def count():
    print("Pred sleep")
    time.sleep(1)  # this will do all counts after each other
    # therefore it will be 4 seconds...
    await asyncio.sleep(1)
    print("Po sleep")


async def main():
    await asyncio.gather(count(), count(), count())
    # this is different:
    await count()
    await count()
    await count()

    # what does gatherer?
    #


if __name__ == "__main__":
    import time

    s = time.perf_counter()
    asyncio.run(main())
    e = time.perf_counter() - s
    print(f"Trvalo to {e:0.2f} sekund.")

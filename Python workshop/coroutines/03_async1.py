import asyncio
i=1
async def gen(u=8):
  """

  :param u:
  :return:
  """
  i = 0
  while i < u:
    yield 2 ** i
    i += 1
    await asyncio.sleep(.3)

async def tisknu_si():
  """
  runs in cycles - always printing
  :return:
  """
  global i
  while True:
    print(f'Neco si tisknu a pocitam {i}')
    i+=1
    await asyncio.sleep(.5)
  #return i

async def main():
  """
  is called from loop.
  :return:
  """
  g = [i async for i in gen()]
  f = [j async for j in gen() if not (j // 3 % 5)]
  print(g, f, sep="\n")
  #return g, f

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.gather( main()  , tisknu_si()  )) ## THIS IS THE HEART
loop.close()

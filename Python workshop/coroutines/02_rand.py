#!/usr/bin/env python3
# rand.py

import asyncio
import random

# ANSI barvy
text_color = (
    "\033[0m",   # Cerna
    "\033[36m",  # Tyrkysova
    "\033[91m",  # Cervena
    "\033[35m",  # Magenta
)

async def makerandom(color_number: int, threshold: int = 6) -> int:
    """
    :summary: This function cycles in while until the threshold is not reached.
    :param idx: color to be chosen from text_color tuple
    :param threshold: threshold to be reached by "randoming"
    :return: returns color, if initial value is wtf...
    """
    print(text_color[color_number] + f"Inicializace makerandom({color_number}).")
    idx = random.randint(0, 10)
    while idx <= threshold:
        print(text_color[color_number] + f"makerandom({idx}) == {idx} ; nizka hodnota, znovu.")
        await asyncio.sleep(0.1)
        idx = random.randint(0, 10)
    print(text_color[color_number] + f"---> Konec: makerandom({idx}) == {idx}" + text_color[0])
    return idx

async def main():

    print('main again')
    res = await asyncio.gather(*(makerandom(i_value+1, 9 - i_value) for i_value in range(3)))
    # wtf do this line?
    # it creates (makerandom(<idx>, <treshold>)
    # therefore gather call is like follows:
    # await asyncio.gather(makerandom(1,8), makerandom(2,7), makerandom(3,6))
    print(f'res value = {str(res)}')
    return res

if __name__ == "__main__":
    #random.seed(250)
    r1, r2, r3 = asyncio.run(main())
    print()
    print(f"r1: {r1}, r2: {r2}, r3: {r3}")

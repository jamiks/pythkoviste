import pdb
import regex as re

name_re = re.compile(r'[A-ZÁČĎÉĚÍŇÓŘŠŤŮÚÝŽ][a-záčďéěíňóřšťůúýž]+(( [A-ZÁČĎÉĚÍŇÓŘŠŤŮÚÝŽ][a-záčďéěíňóřšťůúýž]+)|( [a-záčďéěíňóřšťůúýž]+))*')
name_rere= re.compile(r'[[:upper:][[:lower:]]+(( [[:upper:]][[:lower:]]+)|( [[:lower:]]+))*')
code_re = re.compile(r'^[A-Z][A-Z]$')
sub_whitespaces = re.compile(r'\s')

newre = re.compile(r'([]),([[:upper:]][[:lower:]]+( [[:alpha:]]+)*),()')
test_string_01 = ' CZ  , Město nad Ohří  ,      7814210   '
test_string_02 = ' CZ  , Mesto nad Ohri  ,      7814210   '

city  = name_re.search(test_string_01).captures()
city2 = name_rere.search(test_string_01).captures()

city3  = name_re.search(test_string_02).captures()
city4 = name_rere.search(test_string_02).captures()

print(city)
print(city2)
print(city3)
print(city4)

mesta = {}

with open(r'mesta.txt', 'rt') as f:
    for line in f:
        kod, mesto, obyvatele = line.strip().split(',')
        mesta.setdefault(kod,[]).append((mesto, int(obyvatele)))
        #set default kouka jestli tam uz je hodnota, kdyz ne hodi tam default - tzn: [] v tomhle pripade.
        #ve 2.7 se pouziva default dict z collections.

for ckod in sorted(mesta):
    print(ckod, ': ', ','.join([x[0] for x in sorted(mesta[ckod], key=lambda y:y[1])]), sep='')

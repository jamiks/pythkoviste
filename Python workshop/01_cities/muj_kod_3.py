import pdb

class City:
    def __init__(self, name = 'undefined', population = 0):
        self.name = name
        self.population = int(population)

    def __lt__(self, other):
        return self.population < other.population

    def __eq__(self,other):
        return self.name == other

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

class Country:
    def __init__(self, country):
        self.country = country
        self.cities = []

    def add_city(self, city, population):
        if city in self.cities:
            print(f'City {city} not added, already in list.')
            return False
        self.cities.append(City(city, population))
        self.cities = sorted(self.cities,key=lambda x:x.population) # TODO: lambda used forgetting
        return True

    def __repr__(self):
        return f'{self.country}'

    def __str__(self):
        cities=str(self.cities).strip('[]')
        return f'{self.country}: {cities}'

class Countries:
    def __init__(self):
        self.countries = {}

    def add_city(self, country, city, population):
        if not country in self.countries.keys():
            self.countries[country] = Country(country)

        self.countries[country].add_city(city, population)

    def show(self):
        for country in self.countries.keys():
            print(self.countries[country])

countries = Countries()

with open(r'mesta.txt', 'rt') as f:
    for line in f:
        code, city, population = line.strip().split(',')
        countries.add_city(code, city, population)
        #set default kouka jestli tam uz je hodnota, kdyz ne hodi tam default - tzn: [] v tomhle pripade.
        #ve 2.7 se pouziva default dict z collections.

countries.show()
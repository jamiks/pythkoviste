
filename = 'mesta.txt'#input('Insert filename to be parsed:\n')
divisor = ','#input('Set divisor of data in file:\n')

with open(filename, 'r') as opened_file:
    lines = opened_file.read().split('\n')

all_cities = {}

for line in lines:
    if line == '':
        break

    if line.count(divisor) == 2:
        country, city, population = line.split(divisor)
    else:
        print('Wrong values %s' % (line))
        break

    # check type
    try:
        population = int(population)
    except ValueError:
        print('Population is not integer. Actual value %s' % (population))

    if not country in all_cities.keys():
        all_cities[country] = []

    all_cities[country].append((population,str(city)))

for country in sorted(all_cities):
    country_cities = sorted(all_cities[country])
    out_string = 'country : '
    for city in country_cities:
        out_string += city[1] + ', '
    print(out_string)
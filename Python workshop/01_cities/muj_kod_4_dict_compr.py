with open('mesta.txt', 'r') as f_mesta:
    lines = [line_string.strip('\n').split(',') for line_string in f_mesta] # cajk
    print(lines)

# WRONG mesta = {list_value[0]:(list_value[1],int(list_value[2])) for list_value in lines}
mesta = {code:[x_city for x_code,x_city in lines if x_code == code] for code, value in lines}

print(mesta)
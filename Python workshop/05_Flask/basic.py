from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
  return '<body bgcolor="blue"><h1>Hello World!</h1></body>'

if __name__ == "__main__":
  app.run(debug=True, port=80) #host="0.0.0.0", port=8080
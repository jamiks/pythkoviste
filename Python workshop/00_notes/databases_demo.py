print("""
sqlite3
""")

import sqlite3
import random

with sqlite3.connect(r".\docs\seznam.db") as conn:
  cursor=conn.cursor()
  cursor.execute("select * from employees")
  for record in cursor.fetchall():
    print("Name : %s, phone number : %s" %(record[0],record[1]))
  cursor.close()
  print('\n')
  cur = conn.cursor()
  names = ['John', 'Carl', 'Deph', 'Shizzle', 'Dick', 'Donald', 'Arnold', 'Bruce', 'Bill', 'Tori', 'Jane', 'Ted', 'Martin']
  surnames = ['Campbell', 'Newman', 'Shintaz', 'Miller', 'Kruger', 'Wick']
  phone = random.randrange(600000000,900000000)
  name = f'{names[random.randrange(0,len(names))]} {surnames[random.randrange(0,len(surnames))]}'
  cur.execute(f'insert into employees values ("{name}",{phone})')
  conn.commit()
  cur.close()

  cur2 = conn.cursor()
  cur2.execute("select * from employees")
  for record in cur2.fetchall():
    print("Name : %s, phone number : %s" % (record[0], record[1]))

  """
  For connecting to remote library (db) I may need some client etc. 
  - not my case. I don't need it.. see priklady db5.py
  """
"""
Are all values strings? (or any criterion)
"""
my_dict = {
    1:'a',
    2:'b',
    'q':7,
    1.1:None
}

def check_types(my_type, in_dict):
    return all(not isinstance(item, my_type) for item in in_dict.values())

checked_types = [type(None), int, str, bool, dict, list, set, frozenset, tuple, object]

for ch_type in checked_types:
    if check_types(ch_type, my_dict):
        print(f'!!!! Type {ch_type} is NOT in My dictionary')
    else:
        print(f'Type {ch_type} is in My dictionary')

old_list = [1,5,24,6,7,0,-14,2,7]

filtered_list = [i for i in old_list if i > 5]
print(filtered_list)
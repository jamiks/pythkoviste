"""
Lambda is operation for working with lists, tuples etc...
"""

def not_lambda(x):
    return x[-1]

is_lambda = lambda x:x[-1]

# i.e. lambda [input var]:function

a=[(5, 4.7), (7.1, 7.8), (1,6.1)]

print(is_lambda(a) == not_lambda(a))

print(sorted(a, key=lambda x:x[-1]))
print(sorted(a))
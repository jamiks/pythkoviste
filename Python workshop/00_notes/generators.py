L = range(4)
y=(x**2 for x in range(4) if x%2 == 0)

print('First')
for i in y:
    print(i)

print('second')
for i in y:
    print(i)
print('list')
z=[x**2 for x in L if x%2 == 0]
print(z)
print('dict')
q = {x:x**2 for x in L if x%2 == 0}
print(q)
print('tuple')
m = tuple(x**2 for x in range(4) if x%2 == 0)

print(m)

# sublists
L1=[list(range(5)),list(range(3))]
L2=[*L1]
print(L2)

L4 = [x**2 for x in range(10) if not x%2 ]

print(L4)

# is the same as:
L5=[]
for i in range(10):
    if not i%2:
        L5.append(i**2)

print(L5)

##
L_fcd = [list(range(3)),list(range(1)), list(range(7))]
L6=[]
for i in L_fcd:
    for j in i:
        L6.append(j)

print(L6)

L7 = [i for sublist in L_fcd for i in sublist]
print(L7)
L8 = [j for i in L_fcd for j in i if j>=0]
print(L8)
print('''
for sublist in L_fcd
  for i in sublist
    i
is it it?
''')
import collections.abc
import pdb
NewClass = type('NewClasss', (), {})

print(f"""
{NewClass}
type(<name>, <inheritance>, <attributes>)

How to create Meta Classes?
""")

class NewMetaClass(type):
    def __init__(cls, classname, bases, dictionary):
        super().__init__(classname, bases, dictionary) # calling type
        assert isinstance(getattr(cls,"some_name"), collections.abc.Callable), (f"class {classname} must provide some_name() method")
        #assert isinstance(getattr(cls,"some_attr"), int), (f"class {classname} must provide some_attr attribute of integer")

class NewClass(metaclass=NewMetaClass):
    def __init__(self, some_attr = 10):
        self.some_attr = some_attr

    def some_name(self):
        pass

## deleting methods in instances will delete the methods in class objects?

""")"""
print(NewClass.__dict__)
pdb.set_trace()

print("""
Testing time performance
timeit - for small code snippets
  >>> pyhton -m timeit
cProfile - profile of code -> getting bottlenecks of code
  >>> cProfile.run(<executable call>)

""")

def fcn_slow():
    out_list = []
    i = 0
    while i<10000:
        out_list.append(sum(out_list) + i)
        i+=1
    return out_list

def set_range(inlist, number):
    return inlist[:number]

def main():
    a=fcn_slow()
    b=set_range(a,10)
    print('Done')

if __name__ == '__main__':
    main()
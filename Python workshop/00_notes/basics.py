import pdb
"""
in __builtins__ are defined basic keywords etc. - print it to see
"""
# in case of following
int = str
del int # correction

"""
assert 1<10 # code will continue
assert 1>10 # code will raise assertionError
"""
test_list = ['a','b','c']

enumerate(test_list) # returns tuples with [(<index>,<value>), ...],
# can have second argument as value - starting index value

# creating 2 lists of values and population
# avoiding
city_list = [('Praha',1000100), ('Brno', 480123), ('Plzen',160000)]

cities, pops = zip(*city_list)
print(cities)
print(pops)
# adding two lists to one
print(list(zip(cities, pops)))
# ! Zip is ITERATOR, not a list, or set.

# what * is doing?
a_list=[12,45,78,12]
print(a_list)
print(*a_list)
a_dict = {'val':77, 'opr': 74}
print(*a_dict)
# what does ** do?
def some_fcn(**kwargs):
    print(kwargs)
some_fcn(**a_dict)
b_dict = {'grt':78}
# dict update without update
c_dict = {**a_dict, **b_dict}
a_dict.update(b_dict) # does pretty much the same.
# working with lists more:
deep_copy = a_list
shallow_copu = a_list[:]
# only on modifiable objects - not int, float, str, tuple etc...

"""
LOCAL/GLOBAL
How functions work with locals:
local/global variables from main, or parent functions, can read them.
To write them, use global variables.
"""

# Basics of instances/types:
a='value'

print(a.upper())
print(str.upper(a))


class FlippingDict(dict):
    """
    not finished - do it by yourself
    """
    def __init__(self, nkeys=0, nvals=0):
        super().__init__(self)
        if nkeys > 0 and nvals > 0:
            keys = list(range(nkeys))
            vals = list(range(nvals))
            for idx, key in enumerate(keys):
                self[key] = vals[idx % (len(vals))]

    def flip(self):
        out_dict = FlippingDict()
        keys = list(self.keys())
        values = list(self.values())
        for idx, value in enumerate(values):
            if isinstance(value, list):
                iter_values = value
            else:
                iter_values = [value]
            key = keys[idx]
            for new_key in iter_values:
                if not new_key in out_dict.keys():
                    out_dict[new_key] = key
                    break
                elif not isinstance(out_dict[new_key], list):

                out_dict[new_key].append(key)
        return out_dict
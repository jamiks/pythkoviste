Pokročilé OOP techniky

    Zopakování OOP v jazyce Python
    Magické metody
    Dedičnost, polymorfismus.
    Properties
    Statika
    Metatřídy

Pokročilé konstrukty jazyka

    Generátory a iterátory
    Generátorová notace
    Coroutines a closures
    Dekorátory
    Funktory

Standardní knihovna, zajímavé moduly a balíčky

    Přehled modulů a balíčků standardní knihovny
    Repozitář PyPi

Základy funkcionálního programování v Pythonu

    Anonymní funkce, first-class funkce, rekurze, closures, ...
    Map, reduce, filter
    Generátorová notace
    Moduly operator, itertools, functools
    Zkrácené logické výrazy

Webové aplikace v Pythonu

    Microframework Flask
    Jednoduchá aplikace ve Flasku, formuláře, SqlAlchemy ORM, atd.
    Šablonovací systém Jinja2

Flask v produkčním nasazení, WSGI, atd.
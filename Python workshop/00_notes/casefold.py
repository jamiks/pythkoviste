"""
Used for case in-sensitive
instead of (x.upper == y.upper)
casefold works for german (for example) - where ss and sharp s are the same.
Used for other than latin or greek characters
"""

a='aKDjanK'
b='AKdJank'
print(a.casefold() == b.casefold())
print(a.casefold())
print(b.casefold())
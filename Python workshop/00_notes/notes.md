# 2to3
* range is not list but range (iterator) - faster
* numbers can be written as 10_000_000 - since 3.6

# single line ops
* lambda functions:
  lambda x:x+1
* for cycles:
  x**2 for x in range(3)
 
 # File operations and CODING (unicode, cp1250 etc..)
 * problems  
 * https://cs.wikipedia.org/wiki/Byte_order_mark
 * long a or other characters can have multiple 
 
 # basics
 * see Python » 3.8.1 Documentation » The Python Language Reference » Simple Statements
 * Python » 3.8.1 Documentation » The Python Language Reference » Compound Statements
 
 # regexs
 * https://regex101.com/
 
 # mmap
 * two processes can access one file/memory, and therefore access changes of other process immediately.
 * exchanging information rapidly
 
 # hodnoceni kurzu
 * https://eprezence.gopas.cz/Eprezence/Profile/Index/8VGykTgb_tP-71D0qK5mBFDXDCCPfr9ge_A8VEq8Ni48-OUfu9UQWg51Q_sp6whJbosNcGWfebw8u-rabDBjivt1X3eYdKcc93wnd8BjzVLTU_XU_ZNItM0sQ790AXqgteZl3wUeuBXzEssrI3OAR2MT2gubBF9C7VLhyCi0h6DGXTtRpg8kTGw-DvHdDJbL
 
 # web frameworks
 * fullstack - Django, web2pt
 * micro - Bottle, CherryPy Flask
 * usage of decorators
 
 # Things to check
 * ORM https://cs.wikipedia.org/wiki/Objektov%C4%9B_rela%C4%8Dn%C3%AD_mapov%C3%A1n%C3%AD
   * Declarative mapping
 * SQLAlchemy
  
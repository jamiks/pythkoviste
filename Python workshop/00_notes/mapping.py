"""
Mapping - using map(function, list/iterable)
"""

L1=[3,78,53,7,51,21]
def na_druhou(x):
    return x**2
# one way
L2 = [na_druhou(y) for y in L1]
print(L2)
# other way
out = map(na_druhou, L1)
print(out)
print(list(out))

out2 = map(lambda x:x**2, L1)
out3 = map(na_druhou, range(5))
print(list(out2))
print(list(out3))

####################
## FILTERS
###################

out4=list(filter(lambda x:x>20, L1))
print(L1)
print(out4)

#####################
## REDUCTION
######################
import functools
out5=functools.reduce(lambda x, y:x + y, [1,2,3,4])
print(out5)
print("""
Uses two inputs. x as first and y as seconf (usually x = list_val[i], y=list_val[i+1] until there is i+1
L1 = [1,2,3,4]
for idx in range(len(L1)-1):
    L1[idx+1] = function_call(L1[idx],L1[idx+1])

""")

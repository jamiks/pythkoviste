def multiplier_n(n):
    def inner(x):
        print(f'base multiplier {n} multiplying {x}')
        return x*n
    return inner

krat3=multiplier_n(3)
krat5=multiplier_n(5)

print("""
krat3=multiplier_n(3)
krat5=multiplier_n(5)

Generator of multiplier functions
- web frameworks..
""")

print(f'krat3(7) : {krat3(7)}')
print(f'krat5(7) : {krat5(7)}')

print("""
Also can be used as multiplier_n(n)(x) see down
""")
print(f'multiplier_n(7)(10) : {multiplier_n(7)(10)}')

"""
when we don't want readable code:

"""

print(f'5+list([lambda x:x+1])[0](7) : {5+list([lambda x:x+1])[0](7)}')
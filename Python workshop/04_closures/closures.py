import pdb

def f():
    a=10
    def f1():
        nonlocal a
        a+=1
    f1()
    return f1



x=f()
y=f()
x()
print(x.__closure__)
print(type(x.__closure__[0].cell_contents))
print(x.__closure__[0].cell_contents)
print(y.__closure__[0].cell_contents)
print('****')
x()
print(x.__closure__[0].cell_contents)
print(y.__closure__[0].cell_contents)
print('****')
x()
y()
print(x.__closure__[0].cell_contents)
print(y.__closure__[0].cell_contents)
print('****')

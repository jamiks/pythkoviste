class FlipDict(dict):
  def flip(self):
    res=self.__class__()
    for k,v in self.items():
      if not v in res:
        res[v] = set()
      res[v].add(k)
    return res
x=FlipDict({1:'a',2:'b',3:'a'})
print(x)
print(x.flip())
print(id(x))
y=x.flip()
print(type(y))
print(id(y))

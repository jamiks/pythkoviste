import collections.abc
class LoadableSaveable(type):
  def __init__(cls, classname, bases, dictionary):
    super().__init__(classname, bases, dictionary)
    assert hasattr(cls, "load") and \
                   isinstance(getattr(cls, "load"), collections.abc.Callable), ("class '" + classname + "' must provide a load() method")
    assert hasattr(cls, "save") and \
                   isinstance(getattr(cls, "save"), collections.abc.Callable), ("class '" + classname + "' must provide a save() method")

class Bad(metaclass=LoadableSaveable):
  def some_method(self):
    pass
  def load(self):
    pass
  save=123

class Good(metaclass=LoadableSaveable):
  def load(self): pass
  def save(self): pass

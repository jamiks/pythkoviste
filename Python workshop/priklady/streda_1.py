#!/usr/bin/env python
# coding: utf-8

# In[21]:


mesta={}
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        #print(kod,mesto,obyvatele,end='')
        try:
            mesta[kod].append((mesto,int(obyvatele)))
        except KeyError:
            mesta[kod]=[(mesto,int(obyvatele))]
print(mesta)
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# In[22]:


mesta={}
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        #print(kod,mesto,obyvatele,end='')
        if kod in mesta:
            mesta[kod].append((mesto,int(obyvatele)))
        else:
            mesta[kod]=[(mesto,int(obyvatele))]
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# In[23]:


mesta={}
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        mesta.setdefault(kod,[]).append((mesto,int(obyvatele)))
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# In[24]:


from collections import defaultdict
mesta=defaultdict(lambda:[])
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        mesta[kod].append((mesto,int(obyvatele)))
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# In[ ]:





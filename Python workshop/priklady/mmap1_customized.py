import mmap
with open("pokus.txt","r+b") as f:
  mapped_file=mmap.mmap(f.fileno(), 0)
  print(f'\nDecoding\n{mapped_file[:].decode()}')
  #print(f'\nDecoding\n{mapped_file[:5].decode()}') #this is what?
  #mapped_file[0:6]= b'WO2345'
  #mapped_file[0:1]= b'X'
  #mapped_file.seek(4)
  print(f'\nReadlining:\n{mapped_file.readline()}')
  print(f'\nTelling :\n{mapped_file.tell()}')

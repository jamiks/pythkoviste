#!/usr/bin/env python
# coding: utf-8

# In[21]:


mesta={}
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        #print(kod,mesto,obyvatele,end='')
        try:
            mesta[kod].append((mesto,int(obyvatele)))
        except KeyError:
            mesta[kod]=[(mesto,int(obyvatele))]
print(mesta)
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# In[22]:


mesta={}
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        #print(kod,mesto,obyvatele,end='')
        if kod in mesta:
            mesta[kod].append((mesto,int(obyvatele)))
        else:
            mesta[kod]=[(mesto,int(obyvatele))]
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# In[23]:


mesta={}
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        mesta.setdefault(kod,[]).append((mesto,int(obyvatele)))
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# In[24]:


from collections import defaultdict
mesta=defaultdict(lambda:[])
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        mesta[kod].append((mesto,int(obyvatele)))
for ckod in sorted(mesta):
    print(ckod,': ',', '.join([x[0] for x in sorted(mesta[ckod],key=lambda y:y[1])]),sep='')


# # priklad 3

# In[25]:


a=34
b=78
print(a*b)


# In[49]:


class Clovek:
    #jmeno="Nezname"
    #vek=0
    def __init__(self,p_jmeno,p_vek=0):
        self.jmeno=p_jmeno
        self.vek=p_vek
    def tiskni(self):
        print('Jmenuji se {} a mam {} roku'.format(self.jmeno,self.vek))
    def pozdrav(this,koho):
        print('Ahoj {} '.format(koho))
    def __del__(self):
        print('Koncime s objektem',id(self))
    def __str__(self):
        return f'Jmenuji se {self.jmeno}'
    def __lt__(self,other):
        return self.vek<other.vek

pepa=Clovek('Josef',40)
#epa.jmeno='Josef'
#pepa.vek=34
#print(pepa.jmeno,pepa.vek)
pepa.tiskni()
#pepa.pozdrav('Honzo')
#Clovek.tiskni(pepa)
honza=Clovek('Jan',23)
honza.tiskni()
print(id(honza.jmeno),id(pepa.jmeno))
print(honza)
x=str(honza)
print(x)
print(honza<pepa)
#print(pepa.nezname)


# In[60]:


class Mesto:
    def __init__(self,nazev,obyvatele=0):
        self.nazev=nazev
        self.obyvatele=int(obyvatele)
    def __str__(self):
        return str(self.nazev)
    def __repr__(self):
        return self.nazev
    def __lt__(self,other):
        return self.obyvatele<other.obyvatele
class Stat:
    def __init__(self,kod):
        self.zkratka=kod
        self.seznam=[]
    def append(self,m):
        if type(m) is Mesto:
            self.seznam.append(m)
        else:
            raise TypeError('Unsupported type for State')
    def __str__(self):
        return self.zkratka+': '+', '.join([str(x) for x in sorted(self.seznam)])
"""    
CZ=[Mesto('Praha',1100000),Mesto('Brno',400000)]
print(sorted(CZ))"""
CZ=Stat('CZ')
CZ.append(Mesto('Praha',1100000))
CZ.append(Mesto('Brno',400000))
print(CZ)
for i in CZ:
    print(i)

mesta={}
with open(r'e:\skoleni\mesta.txt','rt') as f:
    for radek in f:
        kod,mesto,obyvatele=radek.strip().split(',')
        if kod not in mesta:
            mesta[kod]=Stat(kod)
        mesta[kod].append(Mesto(mesto,obyvatele))
    
for ckod in sorted(mesta):
    print(mesta[ckod])









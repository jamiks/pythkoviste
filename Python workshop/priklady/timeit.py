import time
def timeit(fce):
  def obal():
    t1=time.time()
    fce()
    t2=time.time()
    print("Volani trvalo : {}".format(str(t2-t1)))
  return obal

@timeit
def moje_funkce():
  pass

moje_funkce()

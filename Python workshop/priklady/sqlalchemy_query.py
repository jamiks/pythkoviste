from sqlalchemy_declarative import Person, Base, Address
from sqlalchemy import create_engine
engine = create_engine('sqlite:///sqlalchemy_example.db')
Base.metadata.bind = engine
from sqlalchemy.orm import sessionmaker
DBSession = sessionmaker()
DBSession.bind = engine
session = DBSession()
# Make a query to find all Persons in the database
session.query(Person).all()

person = session.query(Person).first()
print(person.name)

session.query(Address).filter(Address.person == person).all()
session.query(Address).filter(Address.person == person).one()
address = session.query(Address).filter(Address.person == person).one()
print(address.post_code,address.street_name)

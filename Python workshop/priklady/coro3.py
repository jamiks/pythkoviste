import aiohttp
import asyncio

async def fetch(session, url):
    async with session.get(url) as response:
        return await response.text()

async def main(url):
    async with aiohttp.ClientSession() as session:
        html = await fetch(session, url)
        print(html)

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.gather(main('http://www.gopas.cz'),main('http://python.org')))

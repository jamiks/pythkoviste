import pdb

class Human:
    def __init__(self, p_name = 'undefined', p_age = 0):
        self.name = p_name
        self.age = p_age

    def __del__(self):
        """
        Method that is called when instance of class is deleted via "del <instance>"
        :return:None
        """
        print(f'Ending object {id(self)}')

    def __str__(self):
        """
        Method thats called when str(<instance>) is called
        :return:
        """
        return f'Human named {self.name} of age {self.age}'

    def __repr__(self):
        """
        Method tha is called when instance is printed or whatever
        :return:
        """
        return 'bla bla'

    def __lt__(self,other):
        """
        Method less than... compares with other - then {instance}>other
        :param other: class with attr age
        :return:
        """
        return self.age < other.age

    def print(self):
        print(f"My name is {self.name.upper()} and I'm {self.age} old")

    def say_hello(self, name):
        print(f"Hello {name}")


pepa = Human('Josef', 10)
honza = Human('Jan', 24)

class MyClass42:
    def __getitem__(self, item):
        print(f'So you wanna {item}, {type(item)}')

a=MyClass42()
a[1:2,7]
# slice object -
s=slice(1:-1) # see slice()


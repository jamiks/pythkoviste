import pdb
"""
generators and yelds

yield - can be compared to return, but workks pretty much differently
"""

def firstn(n):
    # this is actually range(len)
    num = 0
    while num < n:
        yield num # it raises exception stopIteration when cannot go further.. check next(x)
        num += 1

def firstn_return(n):
    num = 0
    while num < n:
        return num
        num += 1

a=firstn(10)
print(a)
print(dir(a))
print(type(a))

try:
    while True:
        print(next(a))
except StopIteration:
    print('StopIteration exception')
except:
    print('Different exception')

L1 = []
for i in firstn(5): L1.append(i)
print(L1)
L2 = [firstn(7)] # will be only generator, not same as above
print(L2)
L3 = list(firstn(5)) # will create list
print(L3)
print("""
What is the difference?
- L1 = ['ahoj'] - creates list with one item (len = 1)
- L2 = list('ahoj') - creates list from string (len = 4)
- L2 = list(10) -cannot be created - needs iterable object!

therefore:
a=[] initializes list, and can be used as [10, 10, 20 ...] with actual values
a=list(<iterator>) - needs iterator, or no input, to create list.

SAME works for Tuple
- except that tuple needs to be defined at least with one comma : a=('value',)
not a=('value')
""")
x=1,2
a=(1,2,3)
b=[8,7]
c=(b)
d=(b,)
printer = [x,a,b,c,d]
for i in printer:
    print(f'{i} - {len(i)}')


"""
@classmethod - parameter should be cls, not self!
##
@staticmethod
##
@property
def <attr>(self)
##
@<attr>.setter
##
@<attr>.delter - whenever I use it del <inst>.<attr>
##
- custom decorators
"""
import functools

@functools.lru_cache(maxsize=10) # cached output of this function for the input, can save time.
def pozdrav_me(jmeno):
    print jmeno
    return "Ahoj "+jmeno


def dekorator_pozdravu(funkce_pozdravu):
    def obal(jmeno):
        return "<b>%s</b>" % (funkce_pozdravu(jmeno))
    return obal

@dekorator_pozdravu
def pozdrav_me2(jmeno):
    return "Ahoj "+jmeno


vyvolej_pozdrav = dekorator_pozdravu(pozdrav_me)
print(vyvolej_pozdrav('Hjaad'))
print(pozdrav_me2('asafe'))

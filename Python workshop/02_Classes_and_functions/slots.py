class S:
    __slots__ = ['jedna', 'dve'] # not that usable now, but it should make program faster
    #def __setattr__(self, key, value):
    #    print(f'Setting {key} to value {value}')
        # self.__dict__[key] = [value] # dictionary is not here, because due to slots, there are specified structure,
        # that cannot be changed...

print(dir(S))
print(S.jedna)
s=S()
s.jedna = 1
s.dve   = 2
s.tri   = 3 # should pass error

print(s.jedna)
print(s.dve)
print(s.tri)

import gc
"""
used for cleaning garbage/memory
weakref are invalid after cleaning gc.collect()
"""

gc.collect()
import pdb
import dataclasses
@dataclasses.dataclass
class DecoratedClass:
    x:int
    y:str

instance_A = DecoratedClass(10,'Hello')

print(instance_A)
pdb.set_trace()
instance_B = DecoratedClass('10',10)
print(instance_B)
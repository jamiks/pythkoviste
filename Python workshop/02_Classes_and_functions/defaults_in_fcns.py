def list_fcn(x=1,L=[]):
    x+=1
    L.append(x)

print(list_fcn.__defaults__) # defaults are saved

L1 =[]

for i in range(3):
    list_fcn(i, L1)

print(f'L1 : {L1}')

for i in range(5):
    list_fcn(i+2)

print(dir(list_fcn)) # f.__defaults__
print(list_fcn.__defaults__) # defaults are saved

print("""
Defaults have saved variables - if mutable, they are changed when using the function - and are saved
You can see above how it works
To avoid it, there must be initialization inside the function e.g.:
def f1(L=None):
  L = L if L == None else []
""")

def f1(x, L=None):
    L = L if type(L) == list else []
    L.append(x)
    print(L)

for i in range(4):
    f1(i)

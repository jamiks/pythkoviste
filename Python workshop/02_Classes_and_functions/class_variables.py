class AttrTest:
    idx = 0
    def __init__(self):
        self.actual_id = self.__class__.idx
        self.__class__.idx += 1




class X:
    def print_hello(self):
        print('Hello')

x=X()
x.print_hello()

del X

y=x.__class__() # creating object according to different class!
y.print_hello()
z = X() # should throw exception
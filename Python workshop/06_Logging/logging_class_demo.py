import logging
import random
import pdb

my_logger = logging.Logger('my_logger')

my_logger.setLevel(logging.INFO)

for i in range(50):
    test_number = random.randrange(0, 100)
    if test_number <10:
        my_logger.exception(f'The value is under 10 [{test_number}]', test_number, i)
    if test_number < 25:
        my_logger.critical(f'The value is under 25 [{test_number}]')
    elif test_number < 40:
        my_logger.error(f'The value is under 40 [{test_number}]')
    elif test_number < 55:
        my_logger.warning(f'The value is under 55 [{test_number}]')
    elif test_number < 70:
        my_logger.info(f'The value is under 70 [{test_number}]')
    elif test_number < 85:
        my_logger.info(f'The value is under 85 [{test_number}]')
    else:
        my_logger.debug(f'The value is over 85 [{test_number}]')
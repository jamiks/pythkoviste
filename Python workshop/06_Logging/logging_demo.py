"""
Setting severity
Module logging - check docs

for further logging info and classes see:

https://docs.python.org/3.8/howto/logging.html or for 3.7
"""

import logging
import random

logging.basicConfig(filename = 'log.ini', level = logging.INFO)

for i in range(50):
    test_number = random.randrange(0,100)
    print(f'Actual number: {test_number}')
    if test_number < 25:
        logging.critical('The value is under 25')
    elif test_number < 40:
        logging.error('The value is under 40')
    elif test_number < 55:
        logging.warning('The value is under 55')
    elif test_number < 70:
        logging.info('The value is under 70')
    elif test_number < 85:
        logging.info('The value is under 85')
    else:
        logging.debug('The value is over 85')


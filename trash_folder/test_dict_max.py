import pytest
import pdb


def test_dictionary():
    max_i = 1000
    run = 0
    while True:
        print(max_i)
        dict = {}
        for i in range(max_i):
            dict.update({i:'value'})
        last_i = 0
        for i in dict.keys():
            if last_i > i:
                pdb.set_trace()
            last_i = i
        run += 1
        assert run < 10**6




test = {
    "hello.world.end":'a',
    "hello.world":'c',
    "hello.world.start":'d',
    "hello":'e',
    "hello.world.middle":'b',
}

out_str = ""

for key in test:
    parent = key[:key.rfind(".")] if "." in key else ""
    if parent not in out_str:
        test.update({key:test[key]})
    else:
        out_str +=f"{test[key]}\n"

print(out_str)
print(test)


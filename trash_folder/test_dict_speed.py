import time


def timeit(fcn, *args, **kwargs):
    start = time.time()
    result = fcn(*args, **kwargs)
    exe_time = time.time() - start
    # print(f"{result}in {exe_time}s")

    return exe_time


def set_value(config, value):
    print(f"setting '{config}' to '{value}'")


def get_dict():
    test_dict = {'conf.section.option': 'value',
                 'conf.section.option2': 'value2',
                 'conf.section2.sption': 'salue',
                 }
    return test_dict


def get_dict_dict():
    test_dict = {'conf': {'section': {'option': 'value',
                                      'option2': 'value2',
                                      },
                          'section2': {'sption': 'salue',
                                       }
                          }
                 }
    return test_dict


SIMPLE = get_dict()
MULTILEVEL = get_dict_dict()


def test_simple_dict():
    for setting, value in SIMPLE.items():
        conf, section, option = setting.split(".")
        print(f"{conf}-{section}-{option} = {value}")
    return True


def test_multi():
    for setting, set_value in MULTILEVEL.items():
        for section, sec_value in set_value.items():
            for option, opt_value in sec_value.items():
                print(f"{setting}-{section}-{option} = {opt_value}")
    return True


def testit(fcn, *args, **kwargs):
    tm_fcn = []
    for i in range(50000):
        tm_fcn.append(timeit(fcn, *args, **kwargs))
    return sum(tm_fcn) / len(tm_fcn)


tst_get_dict = testit(get_dict)
tst_get_dict_dict = testit(get_dict_dict)
tst_test_simple_dict = testit(test_simple_dict)
tst_test_multi = testit(test_multi)

print(f"get_dict: {tst_get_dict}")
print(f"get_dict_dict: {tst_get_dict_dict}")
print(f"test_simple_dict: {tst_test_simple_dict}")
print(f"test_multi: {tst_test_multi}")

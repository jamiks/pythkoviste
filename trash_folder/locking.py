import fcntl
import grp
import os
import stat
import pathlib
import errno

location = pathlib.Path("/tmp/test")
try:
    os.mkdir(location, 777)
except Exception:
    print("folder exists")


def _pid_running(pid: int) -> bool:
    """Check if there is process running with given PID."""
    try:
        os.kill(pid, 0)
    except OSError as excp:
        if excp.errno != errno.ESRCH:  # No such process
            raise
        if excp.errno != errno.EPERM:  # Process is not running under different user
            return False
    return True


def lock(value):
    block = True

    path = location / value
    fileno = os.open(path, os.O_RDWR | os.O_SYNC | os.O_CREAT)

    os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IXOTH)
    os.chown(path, -1, grp.getgrnam("lxd").gr_gid)
    while True:
        try:
            fcntl.flock(fileno, fcntl.LOCK_EX | fcntl.LOCK_NB)
            print("Lock obtained")
        except BlockingIOError:
            print(f"Waiting for lock '{path.name}'.")
            if not block:
                print("not block")
                os.close(fileno)
                fileno = None
                return False
            print("flock")
            fcntl.flock(fileno, fcntl.LOCK_EX)
            print("done")
        print("lseek")
        os.lseek(fileno, 0, os.SEEK_SET)
        if content := os.read(fileno, 2048).decode():
            # We downgrade from exclusive lock to shared one later on. That is implemented in kernel as unlock and
            # only then new lock. After unlock the lock can be assigned to any process waiting for lock. Thus we
            # have to check if the original author is not running and release it if it does.
            if _pid_running(int(content)):
                fcntl.flock(fileno, fcntl.LOCK_UN)
                continue
        os.lseek(fileno, 0, os.SEEK_SET)
        os.truncate(fileno, 0)
        os.write(fileno, str(os.getpid()).encode())
        os.fsync(fileno)
        fcntl.flock(fileno, fcntl.LOCK_SH)  # downgrade lock to allow others to read content
        print(f"Lock acquired {path.name}")
        return True


if __name__ == "__main__":
    lock("testfile")
    input("Press enter to continue...")

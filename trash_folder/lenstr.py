STEP = "lan"
PARAM = "bridge"

my_text = " && ".join([
    "uci set foris.wizard=config",
    "uci add_list foris.wizard.passed=password",
    "uci add_list foris.wizard.passed=profile",
    "uci add_list foris.wizard.passed=networks",
    f"uci add_list foris.wizard.passed={STEP}",
    f"uci set foris.wizard.workflow='{PARAM}'",
    "uci commit foris"
    ])

print(len(my_text))

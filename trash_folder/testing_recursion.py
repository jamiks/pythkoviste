import pytest
import pdb

TEST_PRESET_VALUES = {
    "test":
    {
        "-self": "",
        "test_section":
        {
            "-self": "section",
            "test_option_1": "0",
            "test_option_2": "option value",
            "test_list": ["42", "hello"],
        },
        "@anonymous[0]":
        {
            "-self": "anonymous",
            "test_option_1": "47",
            "test_option_2": "option value",
            "test_list": ["42", "hello"],
        }
    }
}


def get_test_values(test_dict=TEST_PRESET_VALUES):
    """returns values for get_test
    """
    out_list = []
    for key, value in test_dict.items():
        if isinstance(value, dict):
            print(key)
            for up_value in get_test_values(value):
                print(up_value)
                if up_value[1] != 
                    out_list.append((f"{key}.{up_value[0]}", up_value[1],))

        elif key != "-self":
            out_list.append((f"{key}", value))
    return out_list


[0 for i in get_test_values() if print(i)]

from typing import TypedDict

class TestDict(TypedDict):
    integer: int
    string: str
    dictionary: dict
    a_list: list
    a_tuple: tuple

my_dict: TestDict = {
    'integer': 432,
    'string': 'string',
    'dictionary': {'a':1},
    'a_list': [0, 1],
    'a_tuple': (0,),
}

test1: TestDict = {
    'integer': 432.7,
    'string': ['hello'],
    'dictionary': [1,2],
    'a_list': (0, 1),
    'a_tuple': [0,],
}

test2: TestDict = {
    'integer': 432,
    'string': 'string',
    'dictionary': {'a':1},
}

test3: TestDict = {
    'iner': 432,
    'stg': 'string',
    'dinary': {'a':1},
}


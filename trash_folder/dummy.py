import pdb

class DummyClass():
    def __getattribute__(self, name: str):
        def dummy_call(name):
            name = name
            def dummy_fcn(*args, **kwargs):
                print(name)
                print(f"args: {args}")
                print(f"*** kwargs:")
                [0 for i in kwargs if print(i)]
                print(name)
            return dummy_fcn
        
        print(name)
        return dummy_call(name)

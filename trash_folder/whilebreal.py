def get_dict(*in_list: list):
    out_dict = {}
    for ls in in_list:
        out_dict.update({value[0]: value[1] for value in ls})
    return out_dict


a = [(1, 2), (2, 3), ("hell", 4)]
b = [(5, 4), ("q", 3)]

print(get_dict(a, b))

class TestClass:

    __slots__ = ('value', 'type')

    def __init__(self, value, type):
        self.value = value
        self.type = type

    def __str__(self):
        return ": ".join([str(self.type), str(self.value)])

    def __repr__(self):
        return str(self.value)

    def xxxtest(self):
        return 42


import pdb
test = TestClass(1, 'integer')
pdb.set_trace()

my_  
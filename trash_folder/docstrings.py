class Test:
    """Test class for test docstring"""

    def __init__(self, value: int, name: str = "Default"):
        """
        Args:
            value (int): Value to be savedKT
            name (str, optional): Name of this instance. Defaults to "Default".
        """
        self.value = value
        self.name = name


class Test2:
    """Test class for test docstring

    Args:
        value (int): Value to be saved
        name (str, optional): Name of this instance. Defaults to "Default".
    """

    def __init__(self, value: int, name: str = "Default"):
        self.value = value
        self.name = name


import pdb

pdb.set_trace()

file_name = "..//..//xfce4_packages.ini"  # input("File to be processed:\n")

with open(file_name, "r") as in_file:
    lines = in_file.read().split("\n")

output = []


def issection(sections, line):
    test_string = f"{section}: "
    if test_string in line[: len(test_string)]:
        return True
    else:
        return False


new_file = file_name.rsplit(".", maxsplit=1)[0] + ".html"

with open(new_file, "w") as out_file:
    package = None
    description = False
    desc_value = ""
    allowed = ["Breaks", "Replaces", "Depends", "Recommends", "Description"]
    for line in lines:
        if "Package: " == line[:9]:
            if description:
                output.append("\t\t</i>\n</div>")

            description = False
            output.append(f"<h2>{line.split(': ', maxsplit=1)[1]}</h2>")
        if description:
            output.append(f"{line}")
        elif line.strip() and ": " in line:
            section, value = line.split(": ", maxsplit=1)
            if section in allowed:
                output.append(f"\n<div>\n\t<b>{section}</b>: \n\t\t<i>{value}")
                if section == "Description":
                    description = True
                if not description:
                    output.append("\t\t</i>\n</div>")
        elif not line.strip():
            output.append(line)
    out_file.write("\n".join(output))

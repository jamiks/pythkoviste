from dataclasses import dataclass


@dataclass
class TestClass:
    i: int
    s: str
    t: tuple
    l: list


a = TestClass(
    i=78,
    s="a",
    t=(1,),
    l=[1],
)

test1 = TestClass(
    i=1.1,
    s="a",
    t=5,
    l=(2,),
)

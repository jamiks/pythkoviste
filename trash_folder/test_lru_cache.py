from functools import lru_cache
import time

@lru_cache
def fcn(a=0):
    time.sleep(a)
    
if __name__ == "__main__":
    print(0)
    time.sleep(10)
    print(10)
    time.sleep(10)
    print(20)
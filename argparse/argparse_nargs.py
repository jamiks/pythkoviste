import argparse

parser = argparse.ArgumentParser()

parser.add_argument(
    '--foo',
    nargs="+",
    default=[],
)

parser.add_argument(
    '--bar',
    nargs="+",
    default=[],
)


arguments = parser.parse_args()
print()
print(arguments.foo)
print(type(arguments.foo))
print()
print(arguments.bar)
print(type(arguments.bar))
class Father():
    def __init__(self, name, surname, gender = 'Male'):
        self.name = name
        self.surname = surname
        self.gender = gender
        print(self)
    def __repr__(self):
        return f"Father named {self.name} {self.surname} is {self.gender}" 


class Son(Father):
    def __init__(self, name, parent, length):
        self.length = length
        super().__init__(name, parent.surname)
    def __repr__(self):
        return f"Son is named {self.name} {self.surname} is {self.gender} ane long approximately {self.length}"


if __name__=="__main__":
    otec=Father("John", "Snow")
    syn=Son("Marek", otec, 134)
    

import pdb

from typing import Any

class Dummy:
    def __init__(*args, **kwargs):
        pass
    
    def __getattribute__(self, __name: str) -> Any:
        pdb.set_trace(header="getattr")
        return lambda *args, **kwargs: None
    
    def __setattr__(self, __name: str, __value: Any) -> None:
        pdb.set_trace(header="setattr")
        return lambda *args, **kwargs: None
        
        
        
if __name__ == "__main__":
    inst = Dummy("a", key = "value")
    pdb.set_trace(header="main")
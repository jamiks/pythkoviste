import requests

branches = ['hbd', 'hbl', 'hbk', 'hbt', 'hbs']

search_string = 'turris-version_'

print("Actual turris versions on dev branches:\n\n")

for branch in branches:
    packages = requests.get(f'https://repo.turris.cz/{branch}/omnia/packages/base/')
    start = packages.text.find(search_string) + len(search_string)
    stop = start + 5
    if start > 0 and stop > 0:
        version = packages.text[start:stop]
    else:
        version = "Unavailable"
    print(f"{branch}: {version}")

    

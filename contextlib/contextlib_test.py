import contextlib
import pdb
import pytest

class Some(contextlib.AbstractContextManager)

class TestClass:
    def __init__(self, value):
        self.value = value

    def __enter__(self):
        print(f"__enter__ of {self.value}")
        return self

    def __exit__(self, *args, **kwargs):
        arguments = args
        print(f"__exit__ with args:\n{args} \nand  kwargs:\n{kwargs}")

@pytest.fixture()
def context_fixture():
    with contextlib.ExitStack() as stack:
        vals = []
        for i in range(10):
            val = stack.enter_context(TestClass(i))
            vals.append(val)
        yield vals


@pytest.fixture()
def context_fixture_list():
    with contextlib.ExitStack() as stack:
        yield [stack.enter_context(TestClass(i)) for i in range(10)]


def test_context(context_fixture, context_fixture_list):
    pdb.set_trace()


import pdb

netcat=[
"Connection to 10.0.2.1 22 port [tcp/ssh] succeeded!",
"nc: connect to 10.0.2.1 port 24 (tcp) failed: Connection refused",
"nc: port number too large: 67841",
]

ping=[
'PING 10.0.2.1 (10.0.2.1) 56(84) bytes of data.\n64 bytes from 10.0.2.1: icmp_seq=1 ttl=64 time=0.415 ms\n64 bytes from 10.0.2.1: icmp_seq=2 ttl=64 time=0.348 ms\n64 bytes from 10.0.2.1: icmp_seq=3 ttl=64 time=0.302 ms\n\n--- 10.0.2.1 ping statistics ---\n3 packets transmitted, 3 received, 0% packet loss, time 2048ms\nrtt min/avg/max/mdev = 0.302/0.355/0.415/0.046 ms\n',
'PING 10.0.2.7 (10.0.2.7) 56(84) bytes of data.\nFrom 10.0.2.100 icmp_seq=1 Destination Host Unreachable\nFrom 10.0.2.100 icmp_seq=2 Destination Host Unreachable\nFrom 10.0.2.100 icmp_seq=3 Destination Host Unreachable\n\n--- 10.0.2.7 ping statistics ---\n3 packets transmitted, 0 received, +3 errors, 100% packet loss, time 2047ms\npipe 3\n',
]

def parse_nc(stdout):
    return "succeeded!" in stdout

def parse_ping(stdout):
    stdout_lines= stdout.split('\n')
    res_line = [cell for cell in stdout_lines if 'packets transmitted' in cell][0]
    res = [res_str.split(' ',1) for res_str in res_line.strip().split(', ')]
 
    results = {val[1]: val[0] for val in res}
    return "passed" if results.get('packet loss') != '100%' else "failed"
    
print("NETCAT")
[print(f"{parse_nc(i)} -> {i}") for i in netcat]
print("\nPING\n")
[print(f"{parse_ping(i)} -> {i}") for i in ping]
    

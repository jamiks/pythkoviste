


def yield_fcn():
    print("start")
    for i in range(5):
        yield f"Value is {i} - yield value"
        print(f"Value is {i}")
    print("starting second yield")
    for i in range(3):
        yield f"Second yield: {i} - yield value"
        print(f"Second yield: {i}")
    print("***\nend")
        
for i in yield_fcn():
    print("start")
    print(i)
    print("stop")

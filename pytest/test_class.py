import pytest
import pdb
import abc

class Parent(abc.ABC):
    value: int
    other_value: str
    output: int = 0
    other_output: str = "None"

    @pytest.fixture(scope="class")
    def execute_test(self):
        self.output += 5 if self.value < 5 else 1
        self.other_output = "too long" if len(self.other_value) > self.value else "ok"
    
    def test_output(self, execute_test):
        assert self.output > 5
    
    def test_lenght(self, execute_test):
        assert self.other_output == "ok"

class TestGood(Parent):
    value = 70
    other_value = "Hello Dolly"
    output = 0
    other_output = "None"

class TestBadValue(Parent):
    value = -7
    other_value = "asfdg"

class TestBadValueType(Parent):
    value = 11.45
    other_value = "Bla"

class TestNoValue(Parent):
    other_value = "Ooops"

class TestNoString(Parent):
    value = 10

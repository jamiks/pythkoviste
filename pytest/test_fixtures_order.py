import pytest


@pytest.fixture(autouse=True)
def auto3():
    name = "auto3"
    print(f"{name} START")
    yield
    print(f"{name} STOP")


@pytest.fixture()
def nonauto1():
    name = "nonauto1"
    print(f"{name} START")
    yield
    print(f"{name} STOP")


@pytest.fixture(autouse=True)
def auto1():
    name = "auto1"
    print(f"{name} START")
    yield
    print(f"{name} STOP")


@pytest.fixture(autouse=True)
def auto2():
    name = "auto2"
    print(f"{name} START")
    yield
    print(f"{name} STOP")


@pytest.fixture()
def nonauto2():
    name = "nonauto2"
    print(f"{name} START")
    yield
    print(f"{name} STOP")


@pytest.fixture()
def nonauto3():
    name = "nonauto3"
    print(f"{name} START")
    yield
    print(f"{name} STOP")


def test_test(nonauto1, nonauto2, nonauto3):
    print("TEST only nonauto")


def test_test2(nonauto1, nonauto3, nonauto2, auto1, auto3, auto2):
    print("TEST 2")

import os
import pytest
import re

# import pdb


@pytest.fixture
def print_value():
    def __printer(value):
        print(value)
        wwhen = re.search(r"\((.*)\)$", os.environ["PYTEST_CURRENT_TEST"]).group(
            1
        )  # TODO isn't there a
        print(wwhen)
        print(os.environ["PYTEST_CURRENT_TEST"])

    return __printer


def test_main(print_value, record_property):
    record_property("example_key", 1)
    print_value("\nThis is a test\n")
    record_property("example_key", 1)
    # pdb.set_trace(header="Testing.. check `print_value`")
    assert False

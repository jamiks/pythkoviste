import numpy as np

from time import time

ref = np.array(range(10,41,2))
target = np.array(range(10,41,6))

start = time()

res = np.NaN(ref, dtype=np.uint64)
for idx, val in enumerate(target):
    mask = (ref < target[idx+1]) & (ref >= idx)
    res = (mask  & (np.ones_like(ref) * idx))

print(str(time() - start))
print(res)

exit(0)
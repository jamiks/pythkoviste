from dataclasses import dataclass
import pdb

@dataclass
class A:
    C: int
    A: str = 'a'
    B: int = 1
    
if __name__=='__main__':
    pdb.set_trace()